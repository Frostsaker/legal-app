import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

/**
 * Generated class for the DownloadsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-downloads',
  templateUrl: 'downloads.html',
})

export class DownloadsPage {

  public fileTransfer: FileTransferObject = this.transfer.create();

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private transfer: FileTransfer, private file: File) {
  }

  downloadPDF() {
    const url = 'https://visitguatemala.com/asesoriaturistica/regulaciones/reglamento-transito273-98.pdf';
    this.fileTransfer.download(url, this.file.dataDirectory + 'reglamento-transito273-98.pdf')
    .then((entry) => {
      console.log('download complete: ' + entry.toURL());
    }, (error) => {
      // handle error
      console.log(error);
    }).catch((error) => {
      console.log(error);
    });
  }

}
