import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';

/**
 * Generated class for the EmergencyContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-emergency-contact',
  templateUrl: 'emergency-contact.html',
})
export class EmergencyContactPage {

  constructor(private call: CallNumber) {
  }

  async callNumber():Promise<any> {
    try {
      this.call.callNumber('40284947', true);
    }
    catch(e) {
      console.log(e);
    }
  }

}
