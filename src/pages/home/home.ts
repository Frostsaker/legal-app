import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RelatedArticlesPage } from '../related-articles/related-articles';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public items: Array<string>;
  public fequItems: Array<string>;

  ngOnInit() {
    this.setItems();
  }

  setItems() {
    this.fequItems = [
      'Parqueo publico',
      'Puesto de control',
      'Accidente moto',
      'Asaltos'
    ];
    this.items = [
      'Autoridades de transito',
      'Tipo vehiculos',
      'Circulación',
      'Equipamiento básico',
      'Licencias',
      'Normas generales',
      'Parqueo publico',
      'Puesto de control',
      'Accidente moto',
      'Asaltos',
      'Medio ambiente',
      'Transporte publico',
      'Transporte carga',
      'Uso vias publicas',
      'Peatones',
      'Ciclistas',
      'Señalización',
      'Uso del vehiculo'
    ];
  }

  constructor(public navCtrl: NavController) {

  }

  relatedArticles(item) {
    this.navCtrl.push(RelatedArticlesPage, {item: item});
  }

  search(event: any) {
    this.setItems();
    let val = event.target.value;

    if (val && val.trim() !== '') {
      this.items = this.items.filter(function(item) {
        return item.toLowerCase().includes(val.toLowerCase());
      });
    }
  }

}
