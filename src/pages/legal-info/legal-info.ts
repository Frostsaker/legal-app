import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';

/**
 * Generated class for the LegalInfoPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-legal-info',
  templateUrl: 'legal-info.html',
})
export class LegalInfoPage {


  constructor(public navCtrl: NavController, public navParams: NavParams,
    private call: CallNumber) {
  }

  async callNumber(number):Promise<any> {
    try {
      this.call.callNumber(String(number), true);
    }
    catch(e) {
      console.log(e);
    }
  }

}
