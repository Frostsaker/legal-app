import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number';

/**
 * Generated class for the LegalContactPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-legal-contact',
  templateUrl: 'legal-contact.html',
})
export class LegalContactPage {

  public lawyers = [];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private call: CallNumber) {
  }

  ngOnInit() {
    this.setItems();
  }

  setItems() {
    this.lawyers = [
      {'name':'Alejandro Cortes', 'phone':'40284947', 'desc':'Especialista en accidentes de trafico.'},
      {'name':'Freddie Batlle', 'phone':'35180866', 'desc':'Especialista en accidentes con peatones.'},
      {'name':'Ivan', 'phone':'59463892', 'desc':'Especialista en puestos de control.'},
      {'name':'Alejandro Cortes', 'phone':'40284947', 'desc':'Especialista en conducción bajo efectos del alcohol.'},
      {'name':'Freddie Batlle', 'phone':'35180866', 'desc':'Especialista en robo vehiculos.'},
      {'name':'Ivan', 'phone':'59463892', 'desc':'Especialista en traspaso vehicular.'},
    ];
  }

  search(event: any) {
    this.setItems();
    let val = event.target.value;
    // Here send data to DB to let it know a use called the Lawyer.
    if (val && val.trim() !== '') {
      this.lawyers = this.lawyers.filter(function(item) {
        return item.desc.toLowerCase().includes(val.toLowerCase()) || item.name.toLowerCase().includes(val.toLowerCase());
      });
    }
  }

  async callNumber(number):Promise<any> {
    try {
      this.call.callNumber(String(number), true);
    }
    catch(e) {
      console.log(e);
    }
  }

}
