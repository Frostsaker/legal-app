import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReadLawPage } from './read-law';

@NgModule({
  declarations: [
    ReadLawPage,
  ],
  imports: [
    IonicPageModule.forChild(ReadLawPage),
  ],
})
export class ReadLawPageModule {}
