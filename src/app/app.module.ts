import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LegalContactPage } from '../pages/legal-contact/legal-contact';
import { LegalInfoPage } from '../pages/legal-info/legal-info';
import { EmergencyContactPage } from '../pages/emergency-contact/emergency-contact';
import { DownloadsPage } from '../pages/downloads/downloads';
import { ReadLawPage } from '../pages/read-law/read-law';
import { RelatedArticlesPage } from '../pages/related-articles/related-articles';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { CallNumber } from '@ionic-native/call-number';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LegalContactPage,
    LegalInfoPage,
    EmergencyContactPage,
    DownloadsPage,
    ReadLawPage,
    RelatedArticlesPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LegalContactPage,
    LegalInfoPage,
    EmergencyContactPage,
    DownloadsPage,
    ReadLawPage,
    RelatedArticlesPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FileTransfer,
    FileTransferObject,
    File,
    CallNumber,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
